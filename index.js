const fs = require('fs');
const xml2js = require('xml2js');
const parser = new xml2js.Parser();
const builder = new xml2js.Builder();

const filesToBeParsed = [
    '/VolWeb/WebContent/WEB-INF/web.xml',
    '/VolEAR/EarContent/META-INF/content-config.xml',
    '/VolWeb/WebContent/WEB-INF/wsrp-producer-registry.xml'
];

if (process.argv.length <= 2) {
    console.log("Usage: " + __filename + " path/to/directory");
    process.exit(-1);
}

var path = process.argv[2];

let index = -2;
let folder = '';

const doSaveFile = (fullPath, content) => {
    const contentToXML = builder.buildObject(content);
    fs.writeFile(fullPath, contentToXML, 'latin1', (err) => {
        if (err) {
            return console.error(err);
        } else {
            console.log(fullPath, 'has been modified');
        }

    });
};

const doWhenFound = (path) => {
    filesToBeParsed.forEach((file) => {

        fs.readFile(path + file, (err, data) => {
            parser.parseString(data, (err, result) => {
                if (file == '/VolWeb/WebContent/WEB-INF/web.xml') {
                    result["web-app"]["context-param"][0]["param-value"][0] = path + 'VolConfig/config/DEV_CIT/';
                    result["web-app"]["filter-mapping"].forEach(filter => {
                        if( filter["url-pattern"][0].includes('/appmanager/portal') || filter["url-pattern"][0].includes('/servlets/*')) {
                            filter["url-pattern"][0] = filter["url-pattern"][0].replace('/appmanager/portal', '/appmanager/dummypath');
                            filter["url-pattern"][0] = filter["url-pattern"][0].replace('/servlets/*', '/servlets/dummypath/*');
                        }
                    });
                    result["web-app"]["security-constraint"][0]["web-resource-collection"][0]["url-pattern"][0] = 
                    result["web-app"]["security-constraint"][0]["web-resource-collection"][0]["url-pattern"][0].replace('/appmanager/*', '/appmanager/dummypath/*');
                } else if (file == '/VolEAR/EarContent/META-INF/content-config.xml') {
                    result["content-config"]["content-store"][0]["repository-property"][0]["value"][0] = 'hml-consumo.vivo.com.br'
                } else {
                    result["wsrp-producer-registry"]["wsrp-producer"][0]["service-urls"][0]["wsdl-url"][0] = 'http://local-www.vivo.com.br:80/loginmarca/producer.wsdl';
                    result["wsrp-producer-registry"]["wsrp-producer"][1]["service-urls"][0]["wsdl-url"][0] = 'http://local-www.vivo.com.br:80/loginmarca/producer.wsdl';
                }
                doSaveFile(path + file, result);
            })
        });

    });
};

fs.readdir(path, function (err, items) {
    index = items.indexOf('src-meu-vivo-empresas');
    if (index < 0) {
        console.log('No folder found');
        process.exit(-1);
    } else {
        folder = items[index];
        doWhenFound(path + '/' + folder + '/');
    }
});
